.data          # data section
message:      .asciiz "Well done!"
newline:      .asciiz "\n"

.text          # code section

main:
  li $t1, 1             # initialize iteration counter (will be changed by the auto-grader)
  li $t2, 15            # initialize loop limit (will be changed by the auto-grader)
  
################## Don't change the upper part ##################

  ## Your instructions go here  ##

  # Print the $t1 value as long as $t1 is equal to $t2

  # Print the iteration number 
  li $v0, 1
  move $a0, $t1
  syscall
  # Move to the next line
  li $v0, 4
  la $a0, newline
  syscall

################## Don't change the lower part ##################

exit:
  li $v0, 4
  la $a0, message
  syscall

